FROM alpine

MAINTAINER Tony Chang <tony@csp-inc.org>

RUN apk add --update-cache --repository \
    http://dl-3.alpinelinux.org/alpine/edge/testing/ \
	bash \
	gdal=2.1.3-r2 \
	python \
	py-pip \
	py-cffi \
	py-cryptography \
&& pip install --upgrade pip \
&& apk add --virtual build-deps \
	gcc \
	libffi-dev \
	python-dev \
	linux-headers \
	musl-dev \
	openssl-dev \
&& pip install gsutil \
&& apk del build-deps \
&& apk add python3 \
&& pip3 install kml2geojson \
&& rm -rf /var/cache/apk/*
