#!/bin/bash

docker run -it -v $(pwd):/contents -w /contents -e "BOTO_PATH=/contents/.boto" \
	tonychangcsp/gsutil:latest

# first find all the kmz files
gsutil cp gs://blm/*.kmz
# unzip them one at a time to /tmp
# and convert json with k2g
python3 convert_to_json.py

# now push them back to gs://blm/
gsutil cp -r /contents/json gs://blm/
