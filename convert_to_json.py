import zipfile
import os
import kml2geojson as k2g

files = os.listdir('/tmp')
os.mkdir('/tmp/json')

for i in files:
	print(i)
	zip_ref = zipfile.ZipFile('/tmp/%s'%i,'r')
	zip_ref.extractall('/tmp')
	k2g.convert('/tmp/doc.kml', '/tmp/json/%s.json'%(i[:-4]))

